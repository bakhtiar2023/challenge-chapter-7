const { user_games } = require("../db/models");
const md5 = require("md5");

class RegisterModel {
  findByEmail = async (email) => {
    try {
      const data = await user_games.findOne({ where: { email } });
      return data;
    } catch (error) {
      return error;
    }
  };

  findByUsername = async (username) => {
    try {
      const data = await user_games.findOne({ where: { username } });
      return data;
    } catch (error) {
      return error;
    }
  };

  createAccount = async (username, email, password) => {
    try {
      await user_games.create({ username, email, password: md5(password) });
      return true;
    } catch (error) {
      return error;
    }
  };
}

module.exports = new RegisterModel();
