const { user_rooms, user_games, user_histories } = require("../db/models");

class RoomModel {
  findByRoomId = async (roomId) => {
    try {
      const result = await user_rooms.findOne({ where: { id: roomId } });
      return result;
    } catch (error) {
      return undefined;
    }
  };

  updateRoom = async (player2Id, player2Choice, roomId) => {
    try {
      await user_rooms.update(
        { player2Id, player2Choice },
        { where: { id: roomId } }
      );
      return true;
    } catch (error) {
      return undefined;
    }
  };

  findAllRooms = async () => {
    try {
      const data = await user_rooms.findAll({
        attributes: [["id", "roomId"], "roomName", "player1Id", "player2Id"],
        include: [
          {
            model: user_histories,
            as: "resultGames",
            attributes: [["userId", "playerId"], "status", "createdAt"],
          },
          {
            model: user_games,
            as: "player1Games",
            attributes: [
              ["id", "player1Id"],
              ["username", "player1Name"],
            ],
          },
          {
            model: user_games,
            as: "player2Games",
            attributes: [
              ["id", "player2Id"],
              ["username", "player2Name"],
            ],
          },
        ],
      });
      return data;
    } catch (error) {
      return undefined;
    }
  };

  getOneRoom = async (roomId) => {
    try {
      const data = user_rooms.findOne({
        attributes: [["id", "roomId"], "roomName", "player1Id", "player2Id"],
        include: [
          {
            model: user_histories,
            as: "resultGames",
            attributes: [["userId", "playerId"], "status", "createdAt"],
          },
          {
            model: user_games,
            as: "player1Games",
            attributes: [
              ["id", "player1Id"],
              ["username", "player1Name"],
            ],
          },
          {
            model: user_games,
            as: "player2Games",
            attributes: [
              ["id", "player2Id"],
              ["username", "player2Name"],
            ],
          },
        ],
        where: { id: roomId },
      });
      if (data.length < 1) {
        return null;
      }
      return data;
    } catch (error) {
      return undefined;
    }
  };

  findRoomByRoomName = async (roomName) => {
    try {
      const result = await user_rooms.findOne({ where: { roomName } });
      return result;
    } catch (error) {
      return undefined;
    }
  };

  createRooms = async (roomName, player1Id, player1Choice) => {
    try {
      await user_rooms.create({ roomName, player1Id, player1Choice });
      return true;
    } catch (error) {
      return false;
    }
  };
}

module.exports = new RoomModel();
