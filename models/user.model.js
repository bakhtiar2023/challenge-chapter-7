const { user_bios, user_games } = require("../db/models");

class UserModel {
  findBioById = async (userId) => {
    try {
      const data = await user_bios.findOne({ where: { userId } });
      return data;
    } catch (error) {
      return undefined;
    }
  };

  createBiodata = async (
    fullname,
    address,
    phoneNumber,
    dateOfBirth,
    userId
  ) => {
    try {
      await user_bios.create({
        fullname,
        address,
        phoneNumber,
        dateOfBirth,
        userId,
      });
      return true;
    } catch (error) {
      return error;
    }
  };

  updateBiodata = async (
    fullname,
    address,
    phoneNumber,
    dateOfBirth,
    userId
  ) => {
    try {
      await user_bios.update(
        { fullname, address, phoneNumber, dateOfBirth },
        { where: { userId } }
      );
      return true;
    } catch (error) {
      return error;
    }
  };

  getUserBiodata = async (userId) => {
    try {
      const data = await user_bios.findOne({
        attributes: [
          "fullname",
          "address",
          "phoneNumber",
          "dateOfBirth",
          "updatedAt",
        ],
        include: [
          {
            model: user_games,
            as: "user",
            attributes: [["id", "userId"], "username", "email"],
          },
        ],
        where: { userId },
      });
      return data;
    } catch (error) {
      return undefined;
    }
  };

  findAllUsers = async () => {
    try {
      const data = await user_games.findAll({
        attributes: ["id", "username", "email", "createdAt"],
      });
      return data;
    } catch (error) {
      return undefined;
    }
  };
}

module.exports = new UserModel();
