const { user_games } = require("../db/models");
const { Op } = require("sequelize");
const md5 = require("md5");

class LoginModel {
  findByUsername = async (username) => {
    try {
      const data = await user_games.findOne({ where: { username } });
      if (data.length < 1) {
        return null;
      }
      return data;
    } catch (error) {
      return error;
    }
  };

  findByEmail = async (email) => {
    try {
      const data = await user_games.findOne({ where: { email } });
      if (data.length < 1) {
        return null;
      }
      return data;
    } catch (error) {
      return error;
    }
  };

  loginWithUsername = async (userUsername, password) => {
    try {
      const data = await user_games.findOne({
        attributes: { exclude: "password" },
        raw: true,
        where: {
          [Op.and]: [{ username: userUsername }, { password: md5(password) }],
        },
      });
      if (data.length < 1) {
        return null;
      }
      return data;
    } catch (error) {
      return undefined;
    }
  };

  loginWithEmail = async (userEmail, password) => {
    try {
      const data = await user_games.findOne({
        attributes: { exclude: "password" },
        raw: true,
        where: {
          [Op.and]: [{ email: userEmail }, { password: md5(password) }],
        },
      });
      if (data.length < null) {
        return null;
      }
      return data;
    } catch (error) {
      return undefined;
    }
  };
}

module.exports = new LoginModel();
