const { validationResult } = require("express-validator");
const userModel = require("../models/user.model");

class UserController {
  updateBiodata = async (req, res) => {
    const { userId } = req.params;
    const { fullname, address, phoneNumber, dateOfBirth } = req.body;
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res
        .status(400)
        .json({ message: { [errors.array()[0].path]: errors.array()[0].msg } });
    }

    const resultCheck = await userModel.findBioById(userId);
    if (resultCheck === undefined) {
      return res.status(500).json({ message: "Internal server error" });
    } else if (resultCheck === null) {
      const created = await userModel.createBiodata(
        fullname,
        address,
        phoneNumber,
        dateOfBirth,
        userId
      );
      if (created === true) {
        return res.status(200).json({ message: "Biodata successfully added" });
      }
      return res.status(500).json({ message: created });
    }

    const updated = await userModel.updateBiodata(
      fullname,
      address,
      phoneNumber,
      dateOfBirth,
      userId
    );
    if (updated === true) {
      return res.status(200).json({ message: "Biodata successfully added" });
    }
    return res.status(500).json({ message: updated });
  };

  getBiodata = async (req, res) => {
    const { userId } = req.params;

    const biodata = await userModel.getUserBiodata(userId);
    if (biodata === null) {
      return res.status(404).json({ message: "Biodata not found" });
    } else if (biodata === undefined) {
      return res.status(500).json({ message: "Internal server error" });
    }
    return res.status(200).json({ message: biodata });
  };

  getAllUser = async (req, res) => {
    const users = await userModel.findAllUsers();
    if (users !== null) {
      return res.status(200).json({ message: users });
    } else if (users === undefined) {
      return res.status(500).json({ message: "Internal server error" });
    } else {
      return res.status(404).json({ message: "Data not found" });
    }
  };
}

module.exports = new UserController();
