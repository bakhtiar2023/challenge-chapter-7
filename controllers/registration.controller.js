const { validationResult } = require("express-validator");
const registerValidator = require("../validators/registration.validator");
const registerModel = require("../models/registration.model");
class RegisterController {
  getRegister = async (req, res) => {
    const { username, email, password } = req.body;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res
        .status(400)
        .json({ meesage: { [errors.array()[0].path]: errors.array()[0].msg } });
    }

    const resultUsername = await registerValidator.isUsernameExist(username);
    if (resultUsername === true) {
      return res.status(400).json({ message: "Username already used" });
    } else if (resultUsername === undefined) {
      return res.status(500).json({ message: "Internal server error" });
    }

    const resutlEmail = await registerValidator.isEmailExist(email);
    if (resutlEmail === true) {
      return res.status(400).json({ message: "Email already used" });
    } else if (resutlEmail === undefined) {
      return res.status(500).json({ message: "Internal server error" });
    }

    const created = await registerModel.createAccount(
      username,
      email,
      password
    );
    if (created === true) {
      return res.status(200).json({ message: "Registration successful" });
    }
    return res.status(500).json({ message: created });
  };
}

module.exports = new RegisterController();
