const { validationResult } = require("express-validator");
const loginModel = require("../models/login.model");
const loginValidator = require("../validators/login.validator");
const jwt = require("jsonwebtoken");
require("dotenv").config();
class LoginController {
  getLogin = async (req, res) => {
    const { username, email, password } = req.body;
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res
        .status(400)
        .json({ meesage: { [errors.array()[0].path]: errors.array()[0].msg } });
    }

    // Login with username
    if (username !== undefined && email === undefined) {
      const resultUsername = await loginValidator.isUsernameRegistered(
        username
      );
      if (resultUsername === false) {
        return res.status(400).json({ message: "Username not valid" });
      } else if (resultUsername === undefined) {
        return res.status(500).json({ message: "Internal server error" });
      }

      const resultLogin = await loginModel.loginWithUsername(
        username,
        password
      );
      if (resultLogin === null) {
        return res.status(400).json({ message: "User not registered" });
      } else if (resultLogin === undefined) {
        return res.status(500).json({ message: "Internal server error" });
      }

      const token = jwt.sign(
        { ...resultLogin, role: "player" },
        process.env.SECRET_KEY,
        { expiresIn: "1d" }
      );
      return res.status(200).json({ accessToken: token });
    }

    // Login with email
    if (username === undefined && email !== undefined) {
      const resultEmail = await loginValidator.isEmailRegistered(email);
      if (resultEmail === false) {
        return res.status(400).json({ message: "Email not valid" });
      } else if (resultEmail === undefined) {
        return res.status(500).json({ message: "Internal server error" });
      }

      const resultLogin = await loginModel.loginWithEmail(email, password);
      if (resultLogin === null) {
        return res.status(400).json({ message: "User not registered" });
      } else if (resultLogin === undefined) {
        return res.status(500).json({ message: "Internal server error" });
      }

      const token = jwt.sign(
        { ...resultLogin, role: "player" },
        process.env.SECRET_KEY,
        { expiresIn: "1d" }
      );
      return res.status(200).json({ accessToken: token });
    }

    return res.status(400).json({ message: "Use username or email for login" });
  };
}

module.exports = new LoginController();
