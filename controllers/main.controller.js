class MainController {
  getMainPage = (req, res) => {
    return res.render("index.html");
  };
  getGamePage = (req, res) => {
    return res.render("games.html");
  };
}

module.exports = new MainController();
