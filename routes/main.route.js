const express = require("express");
const mainRouter = express.Router();
const mainController = require("../controllers/main.controller");

mainRouter.use(express.static("public"));

mainRouter.get("/", mainController.getMainPage);
mainRouter.get("/games", mainController.getGamePage);

module.exports = mainRouter;
