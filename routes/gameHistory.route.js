const express = require("express");
const historyRouter = express.Router();
const historyController = require("../controllers/gameHistory.controller");
const historyValidator = require("../validators/gameHistory.validator");
const authentication = require("../middleware/authMiddleware");
const authorization = require("../middleware/protectionMiddleware");

historyRouter.get(
  "/get_history/:userId",
  authentication,
  authorization,
  historyController.getHistories
);
historyRouter.put(
  "/edit_history/:userId",
  authentication,
  authorization,
  historyValidator.historyValidRules(),
  historyController.createHistory
);

module.exports = historyRouter;
