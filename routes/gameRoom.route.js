const express = require("express");
const roomRouter = express.Router();
const roomController = require("../controllers/gameRoom.controller");
const roomValidator = require("../validators/gameRoom.validator");
const authentication = require("../middleware/authMiddleware");
const paramsChecker = require("../middleware/paramsChecker");

roomRouter.get("/all_rooms", authentication, roomController.getAllRooms);
roomRouter.post(
  "/create_rooms",
  authentication,
  roomValidator.createRoomValidRules(),
  roomController.createRoom
);
roomRouter.get(
  "/get_rooms/:roomId",
  authentication,
  paramsChecker,
  roomController.getRoom
);
roomRouter.put(
  "/edit_rooms/:roomId",
  authentication,
  paramsChecker,
  roomValidator.updateRoomValidRules(),
  roomController.updateRoom
);

module.exports = roomRouter;
