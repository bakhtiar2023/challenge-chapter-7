const express = require("express");
const loginRoute = express.Router();
const loginController = require("../controllers/login.controller");
const loginValidator = require("../validators/login.validator");

loginRoute.post(
  "/login",
  loginValidator.loginValidRules(),
  loginController.getLogin
);

module.exports = loginRoute;
