const express = require("express");
const registerRouter = express.Router();
const registerController = require("../controllers/registration.controller");
const registerValidator = require("../validators/registration.validator");

registerRouter.post(
  "/registration",
  registerValidator.registrationValidRules(),
  registerController.getRegister
);

module.exports = registerRouter;
