const express = require("express");
const userRouter = express.Router();
const userController = require("../controllers/user.controller");
const userValidator = require("../validators/user.validator");
const authentication = require("../middleware/authMiddleware");
const authorization = require("../middleware/protectionMiddleware");

userRouter.get(
  "/get_biodata/:userId",
  authentication,
  authorization,
  userController.getBiodata
);
userRouter.put(
  "/edit_biodata/:userId",
  authentication,
  authorization,
  userValidator.updateBioValidRules(),
  userController.updateBiodata
);
userRouter.get("/all_users", authentication, userController.getAllUser);

module.exports = userRouter;
