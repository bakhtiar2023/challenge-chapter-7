const { body } = require("express-validator");

class UserValidator {
  updateBioValidRules = () => {
    return [
      body("phoneNumber")
        .optional({ nullable: true })
        .isMobilePhone("id-ID")
        .withMessage("Invalid phone number"),
      body("dateOfBirth")
        .optional({ nullable: true })
        .isDate("dd-mm-yyyy")
        .withMessage("Invalid format, input use: dd-mm-yyyy"),
    ];
  };
}

module.exports = new UserValidator();
