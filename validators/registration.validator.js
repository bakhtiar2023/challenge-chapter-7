const { body } = require("express-validator");
const registerModel = require("../models/registration.model");

class RegisterValidator {
  registrationValidRules = () => {
    return [
      body("username").notEmpty().withMessage("Please input your username"),
      body("email")
        .notEmpty()
        .withMessage("Please input your email")
        .isEmail()
        .withMessage("Invalid email format"),
      body("password")
        .notEmpty()
        .withMessage("Please input your password")
        .isLength({ min: 6 })
        .withMessage("Password min.length 6 character"),
    ];
  };

  isEmailExist = async (email) => {
    const result = await registerModel.findByEmail(email);
    if (result === null) {
      return false;
    } else if (result.email.length > 1) {
      return true;
    } else {
      return undefined;
    }
  };

  isUsernameExist = async (username) => {
    const result = await registerModel.findByUsername(username);
    if (result === null) {
      return false;
    } else if (result.username.length > 1) {
      return true;
    } else {
      return undefined;
    }
  };
}

module.exports = new RegisterValidator();
