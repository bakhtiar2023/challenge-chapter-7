const { body } = require("express-validator");
const loginModel = require("../models/login.model");

class LoginValidator {
  loginValidRules = () => {
    return [
      body("username")
        .optional()
        .notEmpty()
        .withMessage("Please input your username"),
      body("email")
        .optional()
        .notEmpty()
        .withMessage("Please input your email")
        .isEmail()
        .withMessage("Invalid email format"),
      body("password").notEmpty().withMessage("Please input your password"),
    ];
  };

  isEmailRegistered = async (email) => {
    const result = await loginModel.findByEmail(email);
    if (result === null) {
      return false;
    }

    if (result.email.length > 1) {
      return true;
    }

    if (result.message.length > 1) {
      return undefined;
    }
  };

  isUsernameRegistered = async (username) => {
    const result = await loginModel.findByUsername(username);
    if (result === null) {
      return false;
    }

    if (result.username.length > 1) {
      return true;
    }

    if (result.message.length > 1) {
      return undefined;
    }
  };
}

module.exports = new LoginValidator();
