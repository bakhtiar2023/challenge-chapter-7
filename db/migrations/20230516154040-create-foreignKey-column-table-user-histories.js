"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.addColumn("user_histories", "roomId", {
      type: Sequelize.INTEGER,
      references: {
        model: "user_rooms",
        key: "id",
      },
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.removeColumn("user_histories", "roomId");
  },
};
