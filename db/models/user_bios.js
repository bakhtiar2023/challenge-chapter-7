"use strict";
const { Model } = require("sequelize");
const moment = require("moment");
module.exports = (sequelize, DataTypes) => {
  class user_bios extends Model {
    static associate(models) {
      user_bios.belongsTo(models.user_games, {as: "user",foreignKey: "userId" });
    }
  }
  user_bios.init(
    {
      userId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      fullname: DataTypes.STRING,
      address: DataTypes.STRING,
      phoneNumber: DataTypes.STRING,
      dateOfBirth: DataTypes.STRING,
      updatedAt: {
        type: DataTypes.DATE,
        get() {
          return moment(this.getDataValue("createdAt")).format(
            "DD/MM/YYYY h:mm:ss"
          );
        },
      },
    },
    {
      sequelize,
      modelName: "user_bios",
    }
  );
  return user_bios;
};
