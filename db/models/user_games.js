"use strict";
const { Model } = require("sequelize");
const moment = require("moment");
module.exports = (sequelize, DataTypes) => {
  class user_games extends Model {
    static associate(models) {
      user_games.hasOne(models.user_bios, { foreignKey: "userId" }),
        user_games.hasMany(models.user_histories, {as: "resultGames", foreignKey: "userId" });
      user_games.hasMany(models.user_rooms, { foreignKey: "player1Id" });
      user_games.hasMany(models.user_rooms, { foreignKey: "player2Id" });
    }
  }
  user_games.init(
    {
      username: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      createdAt: {
        type: DataTypes.DATE,
        get() {
          return moment(this.getDataValue("createdAt")).format("DD/MM/YYYY h:mm:ss");
        },
      },
    },
    {
      sequelize,
      modelName: "user_games",
    }
  );
  return user_games;
};
