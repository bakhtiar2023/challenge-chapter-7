"use strict";
const { Model } = require("sequelize");
const moment = require("moment");
module.exports = (sequelize, DataTypes) => {
  class user_histories extends Model {
    static associate(models) {
      user_histories.belongsTo(models.user_games, {as : "player", foreignKey: "userId" });
      user_histories.belongsTo(models.user_rooms, {as : "gameRoom",foreignKey: "roomId" });
    }
  }
  user_histories.init(
    {
      userId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      status: DataTypes.STRING,
      createdAt: {
        type: DataTypes.DATE,
        get() {
          return moment(this.getDataValue("createdAt")).format(
            "DD/MM/YYYY h:mm:ss"
          );
        },
      },
    },
    {
      sequelize,
      modelName: "user_histories",
    }
  );
  return user_histories;
};
